<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/server', 'MyServerController@index');
Route::post('/server', 'MyServerController@index');

// Auth::routes();

Route::get('/profile', 'HomeController@index')->name('profile');


Route::get('/editprofile/getinfo','HomeController@getInfo');
Route::post('/editprofile/saveinfo','HomeController@saveInfo');
Route::post('/editprofile/saveimage','HomeController@saveImage');
Route::post('/editprofile/savepastwork','HomeController@savePastWork');
Route::post('/editprofile/savepasteducation','HomeController@savePastEducation');
Route::post('editprofile/changepassword','HomeController@changePassword');

Route::get('/editprofile','HomeController@editProfile')->middleware('sso-check');


Route::get('/completeprofile/savegeneralinfo','HomeController@saveGeneralInfo');
Route::get('/completeprofile/saveextrainfo','HomeController@saveExtraInfo');
Route::get('/completeprofile',function(){
    return view('completeprofile');
})->middleware('sso-check');
// Route::get('/editprofile','HomeController@editProfile')->name('editprofile');

