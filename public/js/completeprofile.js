var k = angular.module('completeprofileapp',[]);
k.controller('ProfileController',function($scope,$http){
    $scope.user='';
    $scope.info={};
    $scope.designation='';
    $scope.dob;
    $scope.about;
    $scope.skill;
    $scope.phone;
    $scope.init=function(){
        $scope.user_id=$('#user_id').val();
        $http({
            method : "GET",
            url : "/editprofile/getinfo",
            params: {'id':$scope.user_id}
        }).then(function mySuccess(response) {
            $scope.user=response.data;
            $scope.designation= $scope.user.designation;
            $scope.organization= $scope.user.organization;
            $scope.dob= $scope.user.dob;
            $scope.about = $scope.user.about;
            $scope.phone = $scope.user.phone;
            $scope.skill = $scope.user.skill;
            $('#gender').val($scope.user.gender);
        }, function myError(response) {
        
        });
    }
    $scope.saveGeneralInfo=function($event){
        $event.preventDefault();
        $('#saveGeneralInfoBeforeLoading').attr('hidden','true');
        $('#saveGeneralInfoLoading').removeAttr('hidden');
        $http({
            method : "GET",
            url : "/completeprofile/savegeneralinfo",
            params: {'designation':$scope.designation,'organization':$scope.organization,'dob':$scope.dob,'gender':$('#gender').val()}
        }).then(function mySuccess(response) {
            $scope.user=response.data;
            
            $('#saveGeneralInfoBeforeLoading').removeAttr('hidden');
            $('#saveGeneralInfoLoading').attr('hidden','true');
            
            $('#2ndnxt').trigger('click');
        }, function myError(response) {
            $('#saveGeneralInfoBeforeLoading').removeAttr('hidden');
            $('#saveGeneralInfoLoading').attr('hidden','true');
        });
    }
    $scope.saveExtraInfo=function($event){
        $event.preventDefault();
        $('#saveExtraInfoBeforeLoading').attr('hidden','true');
        $('#saveExtraInfoLoading').removeAttr('hidden');
        $http({
            method : "GET",
            url : "/completeprofile/saveextrainfo",
            params: {'about':$scope.about,'phone':$scope.phone,'skill':$scope.skill}
        }).then(function mySuccess(response) {
            var link = $('#skipandfinish').attr('href');
            $scope.user=response.data;
            window.location.replace(link);
            $('#saveExtraInfoBeforeLoading').removeAttr('hidden');
            $('#saveExtraInfoLoading').attr('hidden','true');
        }, function myError(response) {
            $('#saveExtraInfoBeforeLoading').removeAttr('hidden');
            $('#saveExtraInfoLoading').attr('hidden','true');
        });
    }

});


k.directive("fileread", upload);
upload.$inject=['$http'];

function upload ($http) {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                      var formData = new FormData();
                        formData.append("file",scope.fileread);
                        $('#uploadFileBeforeLoading').attr('hidden','true');
                        $('#uploadFileLoading').removeAttr('hidden');
                        $http.post("/editprofile/saveimage", formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined, 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
                        }).then(function success(response){
                            $('#uploadFileBeforeLoading').removeAttr('hidden');
                            $('#uploadFileLoading').attr('hidden','true');

                           scope.user=response.data;
                           console.log(scope.user.photo);
                           $('[name="uploadedphoto"]').attr('src',scope.user.photo);
                           scope.file='';
                        }, function error(response){
                            // $('#errorMessage').empty();
                            // $('#errorMessage').html(response.data.message);
                            // $('#errorDrive').modal('show');
                                
                            $('#uploadFileBeforeLoading').removeAttr('hidden');
                            $('#uploadFileLoading').attr('hidden','true');
                            scope.file='';
                            });
                    
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
};
