var k = angular.module('editprofileapp',[]);
k.controller('EditProfileController',function($scope,$http){
    $scope.user_id="";
    $scope.user=[];
    $scope.model={'name':false,'about':false,'dob':false,'gender':false,'phone':false,'skill':false,'current_work':false,'current_education':false,'designation':false,'organization':false};
    $scope.tempWork=[];
    $scope.tempEducation=[];
    $scope.error = {'general':null,'oldPassword':null,'password':null};
    $scope.hasPassword;
    $scope.data = {'oldPassword':null,'password':null,'password_confirmation':null};

    $scope.init=function(hasPassword){
        $scope.hasPassword = hasPassword;
        $scope.user_id=$('#user_id').val();
        $http({
            method : "GET",
            url : "/editprofile/getinfo",
            params: {'id':$scope.user_id}
        }).then(function mySuccess(response) {
            $scope.user=response.data;
            if($scope.user.past_work)
            {
                $scope.tempWork=JSON.parse($scope.user.past_work);
            }
            if($scope.user.past_educations)
            {
                $scope.tempEducation=JSON.parse($scope.user.past_educations);
            }
            // $scope.selected=false;
            // setSpaces(response.data);
            // $scope.folders = response.data.folders;
            // $scope.files = response.data.files;
            // $('#renameFolderBeforeLoading').removeAttr('hidden');
            // $('#renameFolderLoading').attr('hidden','true');
                
            
            // $('#renameFolder').modal('hide');
        }, function myError(response) {
            // $('#errorMessage').empty();
            // $('#errorMessage').html(response.data.message);
            // $('#errorDrive').modal('show');
            // $('#renameFolderBeforeLoading').removeAttr('hidden');
            // $('#renameFolderLoading').attr('hidden','true');
        
        });
    }
    $scope.edit=function(key)
    {
        $scope.model[key]=$scope.user[key];
    }

    $scope.cancel=function(key)
    {
        $scope.user[key] = $scope.model[key];
        $scope.model[key]=false;
    }

    $scope.save=function(key)
    {
        $('#save'+key).attr('disabled','disabled');
        $('#save'+key+' i').attr('class','fa fa-spinner fa-spin');
        $('#cancel'+key).attr('disabled','disabled');

        $http({
            method : "POST",
            url : "/editprofile/saveinfo",
            data: {'key':key,'value':$scope.user[key]}
        }).then(function mySuccess(response) {
            $scope.user=response.data;
            $scope.model[key]=false;
            $('#save'+key).removeAttr('disabled');
            $('#save'+key+' i').attr('class','fa fa-check');
            $('#cancel'+key).removeAttr('disabled');
        }, function myError(response) {
            $('#save'+key).removeAttr('disabled');
            $('#save'+key+' i').attr('class','fa fa-check');
            $('#cancel'+key).removeAttr('disabled');
        });
    }
    $scope.addPastWork=function(){
        // var i=$scope.tempWork.length;
        $scope.tempWork.push("");
        // console.log(user.temp);
    }
    $scope.addPastEducation=function(){
        // var i=$scope.tempWork.length;
        $scope.tempEducation.push("");
        // console.log(user.temp);
    }
    $scope.savePastWork=function(){
        // console.log($scope.tempWork);
        $('#savePastWorkLoading').removeAttr('hidden');
        $('#savePastWorkBeforeLoading').attr('hidden','true');
        $http({
            method : "POST",
            url : "/editprofile/savepastwork",
          
            data: {'key':'past_work','value':angular.toJson($scope.tempWork),'_token':$('meta[name="csrf-token"]').attr('content')}
        }).then(function mySuccess(response) {
            $scope.user=response.data;
         
            // $scope.selected=false;
            // setSpaces(response.data);
            // $scope.folders = response.data.folders;
            // $scope.files = response.data.files;
            $('#savePastWorkBeforeLoading').removeAttr('hidden');
            $('#savePastWorkLoading').attr('hidden','true');
                
            if($scope.user.past_work)
            {
                $scope.tempWork=JSON.parse($scope.user.past_work);
            }
            if($scope.user.past_educations)
            {
                $scope.tempEducation=JSON.parse($scope.user.past_educations);
            }
            // $('#renameFolder').modal('hide');
        }, function myError(response) {
            // $('#errorMessage').empty();
            // $('#errorMessage').html(response.data.message);
            // $('#errorDrive').modal('show');
            $('#savePastWorkBeforeLoading').removeAttr('hidden');
            $('#savePastWorkLoading').attr('hidden','true');
        
        });
    }
    $scope.savePastEducation=function(){
        $('#savePastEducationLoading').removeAttr('hidden');
        $('#savePastEducationBeforeLoading').attr('hidden','true');
        $http({
            method : "POST",
            url : "/editprofile/savepasteducation",
            data: {'key':'past_educations','value':angular.toJson($scope.tempEducation),'_token':$('meta[name="csrf-token"]').attr('content')}
        }).then(function mySuccess(response) {
            $scope.user=response.data;
            // $scope.selected=false;
            // setSpaces(response.data);
            // $scope.folders = response.data.folders;
            // $scope.files = response.data.files;
            $('#savePastEducationBeforeLoading').removeAttr('hidden');
            $('#savePastEducationLoading').attr('hidden','true');
                
            if($scope.user.past_work)
            {
                $scope.tempWork=JSON.parse($scope.user.past_work);
            }
            if($scope.user.past_educations)
            {
                $scope.tempEducation=JSON.parse($scope.user.past_educations);
            }
            // $('#renameFolder').modal('hide');
        }, function myError(response) {
            // $('#errorMessage').empty();
            // $('#errorMessage').html(response.data.message);
            // $('#errorDrive').modal('show');
            $('#savePastEducationBeforeLoading').removeAttr('hidden');
            $('#savePastEducationLoading').attr('hidden','true');
        
        });
    }
    $scope.uploadImage=function(){
        
       
    }

    $scope.changePassword=function(){
        $('#submitchangebtn').attr('disabled','disabled');
        $('#passwordChangeSuccess').attr('hidden','hidden');
        $scope.error = {'general':null,'oldPassword':null,'password':null};
        
        $http({
            method : "POST",
            url : "/editprofile/changepassword",
            data: {'oldPassword':$scope.data.oldPassword,'password':$scope.data.password,'password_confirmation':$scope.data.password_confirmation}
        }).then(function mySuccess(response) {
            $scope.resetForm();
            $scope.hasPassword = true;
            $('#submitchangebtn').removeAttr('disabled');
            $('#passwordChangeSuccess').removeAttr('hidden');
            setTimeout(function(){
                $('#passwordChangeSuccess').attr('hidden','hidden');
            }, 5000);
            
        }, function myError(xhr) {
            if(xhr.status == 422){
                $.each( xhr.data.errors, function( key, value ) {
                    $scope.error[key] = value[0];
                });
            }
            else{
                $scope.error.general = 'Something Went Wrong. Please try again.';
            }
            $('#submitchangebtn').removeAttr('disabled');
        });
    }

    $scope.resetForm = function(){
        $scope.data = {'oldPassword':null,'password':null,'password_confirmation':null};
        $scope.error = {'general':null,'oldPassword':null,'password':null};
    }

});

k.directive("fileread", upload);
upload.$inject=['$http'];

function upload ($http) {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                      var formData = new FormData();
                        formData.append("file",scope.fileread);
                        formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
                        $('#uploadTrigger').attr('hidden','true');
                        $('#uploadFileLoading').removeAttr('hidden');
                        $http.post("/editprofile/saveimage", formData, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(function success(response){
                            $('#uploadTrigger').removeAttr('hidden');
                            $('#uploadFileLoading').attr('hidden','true');

                           scope.user=response.data;
                           $('[name="uploadedphoto"]').attr('src',scope.user.photo);
                           scope.file='';
                        }, function error(response){
                            // $('#errorMessage').empty();
                            // $('#errorMessage').html(response.data.message);
                            // $('#errorDrive').modal('show');
                                
                            $('#uploadTrigger').removeAttr('hidden');
                            $('#uploadFileLoading').attr('hidden','true');
                            scope.file='';
                            });
                    
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
};