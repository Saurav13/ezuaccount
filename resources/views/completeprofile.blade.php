@extends('layouts.app')
@section('body')
@section('title','Manage accounts')
<section class="g-py-50" ng-app="completeprofileapp" ng-controller="ProfileController" ng-init="init()">
    <h4 class="title text-center">Complete your profile</h4>
    <br>
    <div id="carousel1" class="js-carousel" data-infinite="true" data-pagi-classes="js-pagination u-carousel-indicators-v30 g-pos-abs g-top-0">
       
        <div class="js-slide g-pt-40 g-pt-150--sm" data-title="Profile Picture">
            <div class="container text-center">
                <h5 class="title text-center">Please select a picture for your profile.</h5>
                <br>
               
                <div class="container" style="display: flex; justify-content: center;">  
                    <br>
                    <div class="u-block-hover" style="max-height:200px;max-width:200px; align:middle !important">
                        <figure >
                            <img ng-if="user.photo!=''"  name="uploadedphoto" class="img-fluid w-100 u-block-hover__main--zoom-v1" ng-src="@{{user.photo}}" alt="Ezuhub">
                            <img ng-if="user.photo==''" name="uploadedphoto" class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{asset('main-assets/assets/img-temp/400x450/img5.jpg')}}"   alt="Ezuhub">
                        </figure>
                                                
                
                            <!-- Figure Caption -->
                        <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                            <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                            <!-- Figure Social Icons -->
                            <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                <li class="list-inline-item align-middle g-mx-7">
                                        
                                <a class="u-icon-v1 u-icon-size--md g-color-white uploadTrigger" href="#!">
                                    <i class="icon-note u-line-icon-pro"></i>
                                </a>
                                </li>
                            
                            </ul>
                            <input type="file" hidden fileread="file" ng-model="file" id="uploadPhoto" onchange="angular.element(this).scope().uploadImage()"/>
                            <!-- End Figure Social Icons -->
                            </div>
                        </figcaption>
                        
                        
                        <!-- End Figure Caption -->
            
                        <!-- User Info -->
                    
                            <!-- End User Info -->
                    </div>
                    <br>
                    
                </div>
                
                <br>
                <p class="" id="uploadFileLoading" hidden  style="display:flex; justify-content:center;">
                    <img   class="" src="{{asset('/loading.gif')}}" style="height: 3rem; width:3rem;"/>
                    
                </p> 
                <a href="javascript:;" id="uploadFileBeforeLoading" class="btn btn-md u-btn-primary g-mr-10 g-mb-15 uploadTrigger">
  			<i class="fa fa-camera-retro"></i> Upload Photo
		</a>
               
                
            </div>
            <br>
            
            <div class="container">
            <a id="1stnxt" href="javascript:;" class="pull-right btn btn-md u-btn-primary u-btn-hover-v1-4 g-font-weight-600 g-letter-spacing-0_5 text-uppercase g-brd-2 g-rounded-50 g-mr-10 g-mb-15">
  		<i class="fa fa-check-circle g-mr-3"></i>
  		Next
	</a>
                
            </div>
        </div>

        <div class="js-slide g-pt-70 g-pt-150--sm" data-title="General Info">
            <div class="container">
            <h5 class="title text-center">Please give your general information.</h5>
                <form class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30" ng-submit="saveGeneralInfo($event)">
                        <!-- Text Input -->
                    <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup1_1">Your Profession</label>
                        <input required  class="form-control form-control-md rounded-0" type="text" ng-model="designation" placeholder="eg. Student, Teacher...">
                    </div>
                    <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup1_1">Your Organizaion/College</label>
                            <input required class="form-control form-control-md rounded-0" type="text" ng-model="organization" placeholder="eg. your school, college, workplace..">
                        </div>
                    
                    <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup1_1">Gender</label>
                        <select class="form-control" id="gender">
                            <option selected>Male</option>
                            <option>Female</option>
                        </select>  
                    </div>
                   
                    <button id="saveGeneralInfoBeforeLoading" type="submit" class="pull-right btn btn-md u-btn-primary u-btn-hover-v1-4 g-font-weight-600 g-letter-spacing-0_5 text-uppercase g-brd-2 g-rounded-50 g-mr-10 g-mb-15"><i class="fa fa-check-circle g-mr-3"></i> Save</button>
                    <p class="" id="saveGeneralInfoLoading" hidden>
                            <img class="" src="{{asset('/loading.gif')}}" style="height: 3rem; width:3rem;"/>
                     </p> 
                     <br>
                    <!-- End Text Input -->
                    
                    <!-- End Text Input with Both Appended Icon -->
                </form>
                
            <br>
            
            <button class="btn btn-primary pull-right" id="2ndnxt" hidden>Next</button>
       
       
       
            </div>

        </div>
        <div class="js-slide g-pt-70 g-pt-150--sm" data-title="About">
            <div class="container">
                    <h5 class="title text-center">Please save your extra information.</h5>
                    <form class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30" ng-submit="saveExtraInfo($event)">
                            <!-- Text Input -->
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10">About me</label>
                            <textarea ng-model="about"  class="form-control form-control-md g-resize-none rounded-0" rows="3" placeholder="Anything that describes you (Optional)"></textarea>
                           
                        </div>
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10" >Contact Number</label>
                            <input ng-model="phone"  class="form-control form-control-md rounded-0" type="text" placeholder="Optional...">
                        </div>
                        <div class="form-group g-mb-20">
                            <label class="g-mb-10">Additional Skills</label>
                            <textarea ng-model="skill" class="form-control form-control-md g-resize-none rounded-0" rows="3" placeholder="Any additional skills you have (Optional)"></textarea>
                            
                        </div>
                        <a id="skipandfinish" href="{{ Request::get('redirectUrl') ? Request::get('redirectUrl') : env('HUB_URL') }}">Skip and finish</a>
                        <button id="saveExtraInfoBeforeLoading" type="submit" class="pull-right btn btn-md u-btn-primary u-btn-hover-v1-4 g-font-weight-600 g-letter-spacing-0_5 text-uppercase g-brd-2 g-rounded-50 g-mr-10 g-mb-15pull-right"><i class="fa fa-check-circle g-mr-3"></i> Save and finish</button>
                        <p class="" id="saveExtraInfoLoading" hidden>
                                <img class="" src="{{asset('/loading.gif')}}" style="height: 3rem; width:3rem;"/>
                         </p> 
                        <!-- End Text Input -->
                        
                        <!-- End Text Input with Both Appended Icon -->
                    </form>
                
            </div>
        </div>

        
    </div>
</section>

<input type="hidden" value="{{Auth::user()->id}}" id="user_id"/>
<input type="hidden" value="{{csrf_token()}}" id="token"/>

@endsection

@section('js')

<script >
  $(document).ready(function () {

    $('#carousel1').slick('setOption', 'customPaging', function (slider, i) {
      var title = $(slider.$slides[i]).data('title');

      return '<i class="u-dot-line-v1 g-brd-gray-light-v2--before g-brd-gray-light-v2--after g-mb-15--sm"><span id="slide'+title.split(' ').join('')+ '" class="u-dot-line-v1__inner g-bg-white g-bg-primary--before g-brd-gray-light-v2 g-brd-primary--active g-transition--ease-in g-transition-0_2"></span></i><span class="g-hidden-sm-down g-color-black g-font-size-15">' + title + '</span>';
    }, true);
  });
</script>
<script>
    $(document).ready(function(){
        $('.uploadTrigger').click(function(){
        $('#uploadPhoto').trigger('click');
        });
        $('#1stnxt').click(function(){
            // console.log($('#slideGeneralInfo').attr('class'));
            $('#slideGeneralInfo').trigger('click');
        })
        $('#2ndnxt').click(function(){
            $('#slideAbout').trigger('click');
        })
    });
    
</script>


<script src="{{asset('/js/completeprofile.js')}}"></script>

@endsection