@extends('layouts.app')
@section('body')
@section('title','Manage accounts')

<style>
    .flexer {
        display:flex;
    }
    
    @media only screen and (min-width : 0px) and (max-width : 767px) { 
        .flexer {
            display:block;
        
    }
</style>
<section class="g-mb-100 g-mt-30" ng-app="editprofileapp" ng-controller="EditProfileController" ng-init="init({{ Auth::user()->password ? 'true' : 'false' }})">
    <div class="container">
      <div class="row">
        <!-- Profile Sidebar -->
        <div class="col-lg-3 g-mb-50 g-mb-0--lg">
          <!-- User Image -->
          <div class="u-block-hover g-pos-rel">
            <figure>
              <img ng-if="user.photo"  name="uploadedphoto" class="img-fluid w-100 u-block-hover__main--zoom-v1" ng-src="@{{user.photo}}" alt="Ezuhub">
              
              <img ng-if="!user.photo" name="uploadedphoto" class="img-fluid w-100 u-block-hover__main--zoom-v1" src="{{asset('main-assets/assets/img-temp/400x450/img5.jpg')}}"   alt="Ezuhub">
            </figure>
                              
            <!-- Figure Caption -->
            <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
              <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                <!-- Figure Social Icons -->
                <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                  <li class="list-inline-item align-middle g-mx-7">
                          
                    <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!" id="uploadTrigger">
                      <i class="icon-note u-line-icon-pro"></i>
                    </a>
                  </li>
                
                </ul>
                <input type="file" hidden fileread="file" ng-model="file" id="uploadPhoto" onchange="angular.element(this).scope().uploadImage()"/>
                <!-- End Figure Social Icons -->
              </div>
            </figcaption>
            <p class="text-center">
                <img hidden id="uploadFileLoading" class="justify-content-center" src="{{asset('/loading.gif')}}" style="height: 3rem; width:3rem;"/>
            </p> 
            
            <!-- End Figure Caption -->

            <!-- User Info -->
            <span class="g-pos-abs g-top-10 g-left-0">
                <a class="btn btn-sm u-btn-primary rounded-0" href="#!">@{{ user.email}}</a>
           
              </span>
            <!-- End User Info -->
          </div>
          <!-- User Image -->

         
         
        </div>
        <!-- End Profile Sidebar -->

        <!-- Profle Content -->
        <div class="col-lg-9">
          <!-- Nav tabs -->
          <ul class="nav nav-justified u-nav-v1-1 u-nav-primary g-brd-bottom--md g-brd-bottom-2 g-brd-primary g-mb-20" role="tablist" data-target="nav-1-1-default-hor-left-underline" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-primary g-mb-20">
            <li class="nav-item">
              <a class="nav-link g-py-10 active" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--1" role="tab">General Info</a>
            </li>
          {{--  <li class="nav-item">
                <a class="nav-link g-py-10" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--3" role="tab">Work/Education</a>
              </li>--}}
            <li class="nav-item">
              <a class="nav-link g-py-10" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--2" role="tab">Security Settings</a>
            </li>
          </ul>
          <!-- End Nav tabs -->

          <!-- Tab panes -->
          <div id="nav-1-1-default-hor-left-underline" class="tab-content">
            <!-- Edit Profile -->
            <div class="tab-pane fade show active" id="nav-1-1-default-hor-left-underline--1" role="tabpanel">
              <h2 class="h4 g-font-weight-300">Manage your General Information</h2>
              <ul class="list-unstyled g-mb-30">
                <!-- Name -->
              
                <li ng-show="model.name===false" class="align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15" style="display:flex;">
                  <div class="g-pr-10">
                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Name</strong>
                  <span class="align-top" ng-click="edit('name')">@{{user.name}}</span>
                  </div>

                  <span>
                    <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1" ng-click="edit('name')"></i>
                  </span>
                </li>
                
                <li ng-hide="model.name===false" class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                  <div id="editposition-box" class="form-group row"  >
                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Name</label>
                    <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                      <div class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                          <input type="text" ng-model="user.name" id="edit_position" class="form-control rounded-0 form-control-md" placeholder="your name" >
                          <span class="input-group-btn">
                            <button class="btn btn-md u-btn-cyan rounded-0" type="button" ng-click="save('name')" id="savename"><i class="fa fa-check"></i></button>
                            <button class="btn btn-md u-btn-red rounded-0" type="button" ng-click="cancel('name')" id="cancelname"><i class="fa fa-times"></i></button>
                          </span>
                      </div>
                    </div>
                  </div>
                </li>
                
                <!-- End Name -->
                
                <!-- Gender -->
                <li ng-show="model.gender===false" class="align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15" style="display:flex;">
                  <div class="g-pr-10">
                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Gender</strong>
                  <span class="align-top" ng-click="edit('gender')">@{{user.gender}}</span>
                  </div>
                  <span>
                    <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1" ng-click="edit('gender')"></i>
                  </span>
                </li>
              
                <li ng-hide="model.gender===false" class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                  <div id="editposition-box" class="form-group row"  >
                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Gender</label>
                    <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                      <div class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                        <select ng-model="user.gender" class="form-control" style="height:39px">
                          <option>Male</option>
                          <option>Female</option>
                        </select>    
                        <span class="input-group-btn">
                          <button ng-click="save('gender')" class="btn btn-md u-btn-cyan rounded-0" type="button" id="savegender"><i class="fa fa-check"></i></button>
                          <button class="btn btn-md u-btn-red rounded-0" type="button" ng-click="cancel('gender')" id="cancelgender"><i class="fa fa-times"></i></button>
                        </span>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- End Gender -->

                <li ng-show="model.designation===false" class="align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15" style="display:flex;">
                  <div class="g-pr-10">
                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Profession</strong>
                  <span class="align-top" ng-click="edit('name')">@{{user.designation}}</span>
                  </div>
                  <span>
                    <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1" ng-click="edit('designation')"></i>
                  </span>
                </li>
              
                <li ng-hide="model.designation===false" class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                  <div id="editposition-box" class="form-group row"  >
                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Designation</label>
                    <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                      <div class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                          <input type="text" ng-model="user.designation" class="form-control rounded-0 form-control-md" placeholder="example: Student, Teacher, Project Manager">
                          <span class="input-group-btn">
                            <button class="btn btn-md u-btn-cyan rounded-0" type="button" ng-click="save('designation')" id="savedesignation"><i class="fa fa-check"></i></button>
                            <button class="btn btn-md u-btn-red rounded-0" type="button" ng-click="cancel('designation')" id="canceldesignation"><i class="fa fa-times"></i></button>
                          </span>
                      </div>
                    </div>
                  </div>
                </li>
              
                <li ng-show="model.organization===false" class="align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15" style="display:flex;">
                  <div class="g-pr-10">
                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Organization</strong>
                  <span class="align-top" ng-click="edit('organization')">@{{user.organization}}</span>
                  </div>
                  <span>
                    <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1" ng-click="edit('organization')"></i>
                  </span>
                </li>
              
                <li ng-hide="model.organization===false" class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                  <div id="editposition-box" class="form-group row"  >
                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Organization</label>
                    <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                      <div class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                          <input type="text" ng-model="user.organization" class="form-control rounded-0 form-control-md" placeholder="example: Pulchowk Campus, Pentagon College">
                          <span class="input-group-btn">
                            <button class="btn btn-md u-btn-cyan rounded-0" type="button" ng-click="save('organization')" id="saveorganization"><i class="fa fa-check"></i></button>
                            <button class="btn btn-md u-btn-red rounded-0" type="button" ng-click="cancel('organization')" id="cancelorganization"><i class="fa fa-times"></i></button>
                          </span>
                      </div>
                    </div>
                  </div>
                </li>  
                
               


                <!-- Phone -->
                <li ng-show="model.phone===false" class="align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15" style="display:flex;">
                  <div class="g-pr-10">
                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Contact number</strong>
                  <span class="align-top" ng-click="edit('phone')">@{{user.phone}}</span>
                  </div>
                  <span>
                    <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1" ng-click="edit('phone')"></i>
                  </span>
                </li>
              
                <li ng-hide="model.phone===false" class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                  <div id="editposition-box" class="form-group row"  >
                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Contact number</label>
                    <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                      <div class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                          <input type="text" ng-model="user.phone"  class="form-control rounded-0 form-control-md">
                            
                        <span class="input-group-btn">
                            <button ng-click="save('phone')" class="btn btn-md u-btn-cyan rounded-0" type="button" id="savephone"><i class="fa fa-check"></i></button>
                            <button class="btn btn-md u-btn-red rounded-0" type="button" ng-click="cancel('phone')" id="cancelphone"><i class="fa fa-times"></i></button>
                          </span>
                      </div>
                    </div>
                  </div>
                </li>
                <!--Phone -->
                 <li ng-show="model.about===false" class="align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15" style="display:flex;">
                  <div class="g-pr-10 flexer">
                    <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10" style="flex:0 0 auto">About Me</strong>
                    <span class="align-top" ng-click="edit('about')" style="flex:1 1 auto; text-align: justify;padding-left: 0.21rem;">@{{user.about}}</span>
                  </div>
                  <span>
                    <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1" ng-click="edit('about')"></i>
                  </span>
                </li>
              
                <li ng-hide="model.about===false" class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                  <div id="editposition-box" class="form-group row"  >
                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">About Me</label>
                    <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                      <div class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                          <textarea ng-model="user.about" id="edit_position" class="form-control rounded-0 form-control-md" placeholder="something about you"></textarea>
                          <span class="input-group-btn">
                            <button class="btn btn-md u-btn-cyan rounded-0" type="button" ng-click="save('about')" id="saveabout"><i class="fa fa-check"></i></button>
                            <button class="btn btn-md u-btn-red rounded-0" type="button" ng-click="cancel('about')" id="cancelabout"><i class="fa fa-times"></i></button>
                          </span>
                      </div>
                    </div>
                  </div>
                </li>
                

               
                <!-- End Website -->
              </ul>

            </div>
            <!-- End Edit Profile -->
          {{--  <div class="tab-pane fade show" id="nav-1-1-default-hor-left-underline--3" role="tabpanel">
              <h2 class="h4 g-font-weight-300">Manage your Work and Education</h2>
            
              <ul class="list-unstyled g-mb-30">
                <!-- Name -->
              
                  <li ng-show="model.current_work===false" class="align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15" style="display:flex;">
                    <div class="g-pr-10">
                      <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Current Occupation</strong>
                    <span class="align-top" ng-click="edit('current_work')">@{{user.current_work}}</span>
                    </div>
                    <span>
                      <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1" ng-click="edit('current_work')"></i>
                    </span>
                  </li>
                
                  <li ng-hide="model.current_work===false" class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                    <div id="editposition-box" class="form-group row"  >
                      <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Current Occupation</label>
                      <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                        <div class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                            <input type="text" ng-model="user.current_work"  class="form-control rounded-0 form-control-md" placeholder="example: Student, Teacher, Project Manager">
                            <span class="input-group-btn">
                              <button class="btn btn-md u-btn-cyan rounded-0" type="button" ng-click="save('current_work')" id="savecurrent_work"><i class="fa fa-check"></i></button>
                              <button class="btn btn-md u-btn-red rounded-0" type="button" ng-click="cancel('current_work')" id="cancelcurrent_work"><i class="fa fa-times"></i></button>
                            </span>
                        </div>
                      </div>
                    </div>
                  </li>


                  <li class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                    <div id="editposition-box" class="form-group row"  >
                      <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Past Work</label>
                      <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                        <div ng-repeat="p in tempWork track by $index" class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                         
                        <textarea class="form-control rounded-0 form-control-md" ng-model="tempWork[$index]">@{{p}}</textarea>
                            <br>
                        </div>
                        <br>
                        <input type="button" class="btn btn-primary" value="Add" ng-click="addPastWork()"/>
                        <img class="pull-right" id="savePastWorkLoading" hidden src="{{asset('/loading.gif')}}" style="height: 3rem; width:3rem;"/>
                        <input type="button" id="savePastWorkBeforeLoading" class="btn btn-success pull-right" ng-show="tempWork.length>0" value="Save" ng-click="savePastWork()"/>
                          </div>
                    </div>
                  </li>
                



                  <li ng-show="model.current_education===false" class="align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15" style="display:flex;">
                    <div class="g-pr-10">
                      <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Current Education</strong>
                    <span class="align-top" ng-click="edit('current_education')">@{{user.current_education}}</span>
                    </div>
                    <span>
                      <i class="icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1" ng-click="edit('current_education')"></i>
                    </span>
                  </li>
                
                  <li ng-hide="model.current_education===false" class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                    <div id="editposition-box" class="form-group row"  >
                      <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Current Education</label>
                      <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                        <div class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                            <input type="text" ng-model="user.current_education"  class="form-control rounded-0 form-control-md" placeholder="">
                            <span class="input-group-btn">
                              <button class="btn btn-md u-btn-cyan rounded-0" type="button" ng-click="save('current_education')" id="savecurrent_education"><i class="fa fa-check"></i></button>
                              <button class="btn btn-md u-btn-red rounded-0" type="button" ng-click="cancel('current_education')" id="cancelcurrent_education"><i class="fa fa-times"></i></button>
                            </span>
                        </div>
                      </div>
                    </div>
                  </li>
                  
                  <li class="g-brd-bottom g-brd-gray-light-v4" style="padding-top:15px;">
                    <div class="form-group row"  >
                      <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-left g-mb-10">Past Education</label>
                      <div class="col-sm-9" style="padding-right:0px; padding-left:0px;">
                        <div ng-repeat="p in tempEducation track by $index" class="input-group g-brd-primary--focus" style="transition: all 0.5s ease-in-out; animation-iteration-count: 1;">
                         
                            <textarea class="form-control rounded-0 form-control-md"ng-model="tempEducation[$index]" >@{{p}}</textarea>
                            <br>
                        </div>
                        <br>
                        <input type="button" class="btn btn-primary" value="Add" ng-click="addPastEducation()"/>
                        <img class="pull-right" hidden id="savePastEducationLoading" src="{{asset('/loading.gif')}}" style="height: 3rem; width:3rem;"/>
                        <input type="button" id="savePastEducationBeforeLoading" class="btn btn-success pull-right" ng-show="tempEducation.length>0" value="Save" ng-click="savePastEducation()"/>
                          </div>
                    </div>
                  </li>
                <!-- End Website -->
              </ul>

            </div> --}}
            <!-- Security Settings -->
            <div class="tab-pane fade" id="nav-1-1-default-hor-left-underline--2" role="tabpanel">
              <h2 class="h4 g-font-weight-300">Security Settings</h2>
              <p class="g-mb-25">Reset your password</p>

              <form ng-submit="changePassword()">
                <p style="text-align:  center;color: red;" ng-show="error.general.length > 0">@{{ error.general }}</p>
                <p style="text-align:  center;color: green;" id="passwordChangeSuccess" hidden>Password was Successfully Changed.</p>
                <!-- Current Password -->
                <div class="form-group row g-mb-25" ng-if="hasPassword">
                  <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Current password</label>
                  <div class="col-sm-9">
                    <div class="input-group g-brd-primary--focus">
                      <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" ng-model="data.oldPassword" placeholder="Current password">
                      <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                        <i class="icon-lock"></i>
                      </div>
                    </div>
                    <p style="text-align:  center;color: red;" ng-show="error.oldPassword.length > 0">@{{ error.oldPassword }}</p>
                  </div>
                </div>
                <!-- End Current Password -->

                <!-- New Password -->
                <div class="form-group row g-mb-25">
                  <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">New password</label>
                  <div class="col-sm-9">
                    <div class="input-group g-brd-primary--focus">
                      <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" ng-model="data.password" placeholder="New password">
                      <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                        <i class="icon-lock"></i>
                      </div>
                    </div>
                    <p style="text-align:  center;color: red;" ng-show="error.password.length > 0">@{{ error.password }}</p>
                  </div>
                </div>
                <!-- End New Password -->

                <!-- Verify Password -->
                <div class="form-group row g-mb-25">
                  <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Verify password</label>
                  <div class="col-sm-9">
                    <div class="input-group g-brd-primary--focus">
                      <input class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0" type="password" ng-model="data.password_confirmation" placeholder="Verify password">
                      <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                        <i class="icon-lock"></i>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End Verify Password -->

                <!-- Login Verification -->
                {{-- <div class="form-group row g-mb-25">
                  <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Login verification</label>
                  <div class="col-sm-9">
                    <label class="form-check-inline u-check g-pl-25">
                      <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                      <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                        <i class="fa" data-check-icon="&#xf00c"></i>
                      </div>
                      Verify login requests
                    </label>
                    <small class="d-block text-muted">You need to add a phone to your profile account to enable this feature.</small>
                  </div>
                </div> --}}
                <!-- End Login Verification -->

                <!-- Password Reset -->
                {{-- <div class="form-group row g-mb-25">
                  <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Password reset</label>
                  <div class="col-sm-9">
                    <label class="form-check-inline u-check g-pl-25">
                      <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                      <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                        <i class="fa" data-check-icon="&#xf00c"></i>
                      </div>
                      Require personal information to reset my password
                    </label>
                    <small class="d-block text-muted">When you check this box, you will be required to verify additional information before you can request a password reset with just your email address.</small>
                  </div>
                </div> --}}
                <!-- End Password Reset -->

                <!-- Save Password -->
                {{-- <div class="form-group row g-mb-25">
                  <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Save password</label>
                  <div class="col-sm-9">
                    <label class="form-check-inline u-check mx-0">
                      <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="savePassword" type="checkbox">
                      <div class="u-check-icon-radio-v7">
                        <i class="d-inline-block"></i>
                      </div>
                    </label>
                    <small class="d-block text-muted">When you check this box, you will be saved automatically login to your profile account. Also, you will be always logged in all our services.</small>
                  </div>
                </div> --}}
                <!-- End Save Password -->

                <hr class="g-brd-gray-light-v4 g-my-25">

                <div class="text-sm-right">
                  <a class="btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10" href="#!" ng-click="resetForm()">Cancel</a>
                  <button type="submit" class="btn u-btn-primary rounded-0 g-py-12 g-px-25" id="submitchangebtn">Save Changes</button>
                </div>
              </form>
            </div>
            <!-- End Security Settings -->

        
            <!-- Notification Settings -->
            <div class="tab-pane fade" id="nav-1-1-default-hor-left-underline--4" role="tabpanel">
              <h2 class="h4 g-font-weight-300">Manage your Notifications</h2>
              <p class="g-mb-25">Below are the notifications you may manage.</p>

              <form>
                <!-- Email Notification -->
                <div class="form-group">
                  <label class="d-flex align-items-center justify-content-between">
                    <span>Email notification</span>
                    <div class="u-check">
                      <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="emailNotification" type="checkbox" checked>
                      <div class="u-check-icon-radio-v7">
                        <i class="d-inline-block"></i>
                      </div>
                    </div>
                  </label>
                </div>
                <!-- End Email Notification -->

                <hr class="g-brd-gray-light-v4 g-my-20">

                <!-- Comments Notification -->
                <div class="form-group">
                  <label class="d-flex align-items-center justify-content-between">
                    <span>Send me email notification when a user comments on my blog</span>
                    <div class="u-check">
                      <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="commentNotification" type="checkbox">
                      <div class="u-check-icon-radio-v7">
                        <i class="d-inline-block"></i>
                      </div>
                    </div>
                  </label>
                </div>
                <!-- End Comments Notification -->

                <hr class="g-brd-gray-light-v4 g-my-20">

                <!-- Update Notification -->
                <div class="form-group">
                  <label class="d-flex align-items-center justify-content-between">
                    <span>Send me email notification for the latest update</span>
                    <div class="u-check">
                      <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="updateNotification" type="checkbox" checked>
                      <div class="u-check-icon-radio-v7">
                        <i class="d-inline-block"></i>
                      </div>
                    </div>
                  </label>
                </div>
                <!-- End Update Notification -->

                <hr class="g-brd-gray-light-v4 g-my-25">

                <!-- Message Notification -->
                <div class="form-group">
                  <label class="d-flex align-items-center justify-content-between">
                    <span>Send me email notification when a user sends me message</span>
                    <div class="u-check">
                      <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="messageNotification" type="checkbox" checked>
                      <div class="u-check-icon-radio-v7">
                        <i class="d-inline-block"></i>
                      </div>
                    </div>
                  </label>
                </div>
                <!-- End Message Notification -->

                <hr class="g-brd-gray-light-v4 g-my-25">

                <!-- Newsletter Notification -->
                <div class="form-group">
                  <label class="d-flex align-items-center justify-content-between">
                    <span>Receive our monthly newsletter</span>
                    <div class="u-check">
                      <input class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="newsletterNotification" type="checkbox">
                      <div class="u-check-icon-radio-v7">
                        <i class="d-inline-block"></i>
                      </div>
                    </div>
                  </label>
                </div>
                <!-- End Newsletter Notification -->

                <hr class="g-brd-gray-light-v4 g-my-25">

                <div class="text-sm-right">
                  <a class="btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10" href="#!">Cancel</a>
                  <a class="btn u-btn-primary rounded-0 g-py-12 g-px-25" href="#!">Save Changes</a>
                </div>
              </form>
            </div>
            <!-- End Notification Settings -->
          </div>
          <!-- End Tab panes -->
        </div>
        <!-- End Profle Content -->
      </div>
    </div>
  </section>
<input type="hidden" value="{{Auth::user()->id}}" id="user_id"/>
<input type="hidden" value="{{csrf_token()}}" id="token"/>

@endsection


@section('js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.2/angular.min.js"></script>  --}}


<script src="{{asset('/js/editprofile.js')}}"></script>
<script>
  $(document).ready(function(){
    $('#uploadTrigger').click(function(){
      $('#uploadPhoto').trigger('click');
    });
  });
</script>
@endsection