
<body id="main_body">
  <div class="se-pre-con">
    {{-- <blockquote style="color:black;">Quotation blah blah blah<p>Joe Bloggs</p></blockquote> --}}
    </div>
  <main>
    <!-- Header -->
    <header id="js-header" class="u-header u-header--static u-header--show-hide u-header--change-appearance u-header--has-hidden-element" data-header-fix-moment="500" data-header-fix-effect="slide">
      <!-- Top Bar -->
      <div class="u-header__section u-header__section--light g-bg-white g-color-main g-brd-bottom g-brd-gray-light-v4 g-py-10">
        <div class="container">
          <div class="row flex-column flex-md-row justify-content-between align-items-center text-center g-font-size-13">
            <div class="col-auto col-lg-6 text-lg-left">
              <ul class="d-inline-block list-inline g-valign-middle mb-0">
                <li class="list-inline-item g-mr-20--lg">
                  EzuAccount
                </li>
                <li class="list-inline-item g-mr-20--lg">
                  <i class="icon-call-in g-mt-1 g-mr-5"></i>
                  Manage your Profile.
                </li>
              </ul>
            </div>

            <div class="col-auto col-lg-6 text-lg-right">
              @if(Request::segment(1) != 'completeprofile')
              	<ul class="d-inline-block list-inline g-valign-middle mb-0">
              
                  <li class="list-inline-item g-mr-20--lg">
                    <a href="{{ Request::get('redirectUrl') ? Request::get('redirectUrl') : env('HUB_URL') }}" style="color:#555">
                    <i class="fa fa-arrow-left g-mt-1 g-mr-5"></i>
                    Return Back
                    </a>
                  </li>
               
              	</ul>
              @endif
              {{-- <ul class="d-inline-block list-inline g-valign-middle mb-0">
                <li class="list-inline-item">
                  <a class="g-color-main g-color-primary--hover g-text-underline--none--hover g-pa-5" href="#!">
                    <i class="icon-home"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="g-color-main g-color-primary--hover g-text-underline--none--hover g-pa-5" href="#!">
                    <i class="icon-globe"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="g-color-main g-color-primary--hover g-text-underline--none--hover g-pa-5" href="#!">
                    <i class="icon-basket"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="g-color-main g-color-primary--hover g-text-underline--none--hover g-pa-5" href="#!">Quicklinks</a>
                </li>
                <li class="list-inline-item">
                  <a class="g-color-main g-color-primary--hover g-text-underline--none--hover g-pa-5" href="#!">My Account</a>
                </li>
              </ul> --}}
            </div>
          </div>
        </div>
      </div>
      <!-- End Top Bar -->
    </header>
    <!-- End Header -->


    
    