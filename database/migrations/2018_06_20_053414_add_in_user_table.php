<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('about')->nullable();
            $table->string('dob')->nullable();
            $table->enum('gender',['Male','Female']);
            $table->text('past_educations')->nullable();
            $table->text('past_work')->nullable();
            $table->text('current_education')->nullable();
            $table->text('current_work')->nullable();
            $table->string('photo')->nullable();
            $table->string('skill')->nullable();
            $table->string('phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
