<?php
/**
 * Created by PhpStorm.
 * User: awnali
 * Date: 6/14/17
 * Time: 3:57 PM
 */

namespace App;

use Jasny\SSO\Server;
use Jasny\ValidationResult;
use Illuminate\Support\Str;
use App\User;
use Auth;
use Socialite;
use Image;

class MySSOServer extends Server
{

    /**
     * Registered brokers
     * @var array
     */
    private static $brokers = [
        'EzuDrive' => ['secret'=>'8iwzik1bwd'],
        'EzuHub' => ['secret'=>'7pypoox2pc'],
        'EzuNote' => ['secret'=>'ceda63kmhp'],
        'EzuCareer' => ['secret'=>'dYXnAwzYz3'],
        'EzuMaster' => ['secret'=>'KlopYu89Sf'],
    ];

    /**
     * Get the API secret of a broker and other info
     *
     * @param string $brokerId
     * @return array
     */
    protected function getBrokerInfo($brokerId)
    {
        return isset(self::$brokers[$brokerId]) ? self::$brokers[$brokerId] : null;
    }

    /**
     * Authenticate using user credentials
     *
     * @param string $username
     * @param string $password
     * @return ValidationResult
     */
    protected function authenticate($username, $password)
    {
        if (!isset($username)) {
            return ValidationResult::error("username isn't set");
        }

        if (!isset($password)) {
            return ValidationResult::error("password isn't set");
        }

        if(Auth::attempt(['email' => $username, 'password' => $password])){
            return ValidationResult::success();
        }
        return ValidationResult::error("can't find user");

    }

    public function login()
    {
        $this->startBrokerSession();

        if (empty($_POST['username'])) $this->fail("No username specified", 400);
        if (empty($_POST['password'])) $this->fail("No password specified", 400);

        $validation = $this->authenticate($_POST['username'], $_POST['password']);

        if ($validation->failed()) {
            return $this->fail($validation->getError(), 400);
        }
        
        $user = User::where('email',$_POST['username'])->first();

        if($user->verified == 0){
            header('Content-type: application/json; charset=UTF-8');
            echo json_encode($user);
        }else{
            Auth::login($user,true);
            $this->setSessionData('sso_user', $_POST['username']);
            $this->userInfo();
        }
    }

    public function logout()
    {
        $this->startBrokerSession();

        if($user = $this->getUserInfo($this->getSessionData('sso_user'))){

            // $user->remember_token = Str::random(60);
            // $user->save();
            Auth::login($user,true);
            Auth::guard()->logout();
        }
        $this->setSessionData('sso_user', null);
        
        header('Content-type: application/json; charset=UTF-8');
       
        http_response_code(204);
    }

    public function register()
    {
        $this->startBrokerSession();

        if (empty($_POST['name'])) $this->fail("No username specified", 400);
        if (empty($_POST['email'])) $this->fail("No email specified", 400);
        if (empty($_POST['password'])) $this->fail("No password specified", 400);

        $existing = User::where('email', $_POST['email'])->first();
        
        if(!$existing){
            $existing= User::create([
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'password' => bcrypt($_POST['password']),
                'confirmation_code' => Str::random(60)
            ]);
        }
        else
        {
            $this->fail("Email has already been taken", 422);
        }

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($existing);
    }

    public function setEmail(){
        $this->startBrokerSession();

        if (empty($_POST['provider'])) $this->fail("No provider specified", 400);
        if (empty($_POST['email'])) $this->fail("No email specified", 400);
        if (empty($_POST['provider_id'])) $this->fail("No provider_id specified", 400);

        $existing = User::where('email', $_POST['email'])->first();
        
        if(!$existing){
            $existing = User::where('provider', $_POST['provider'])->where('provider_id', $_POST['provider_id'])->first();
            if($existing){
                $existing->email = $_POST['email'];
                $existing->save();
            }
        }
        else
        {
            $this->fail("Email has already been taken", 422);
        }

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($existing);
    }


    public function checkUserEmail(){
        $this->startBrokerSession();

        if (empty($_POST['email'])) $this->fail("No email specified", 400);

        $user = $this->getUserInfo($_POST['email']);
        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($user);
    }

    public function confirmUser(){
        $this->startBrokerSession();
        
        if (empty($_POST['token'])) $this->fail("No token specified", 400);

        $user = User::where('confirmation_code',$_POST['token'])->first();
        
        if($user){
            if(!$user->verified){
                $user->verified = 1;
                $user->save();

                Auth::login($user,true);

                $this->setSessionData('sso_user', $user->email);
                $this->userInfo();
            }
        }
	    else{
        	header('Content-type: application/json; charset=UTF-8');
        	echo json_encode($user);
        }
    }

    public function resetPassword(){
        $this->startBrokerSession();

        if (empty($_POST['email'])) $this->fail("No email specified", 400);
        if (empty($_POST['password'])) $this->fail("No password specified", 400);

        $user = $this->getUserInfo($_POST['email']);

        if($user){
            $user->password = bcrypt($_POST['password']);
            $user->verified = 1;
            $user->save();

            Auth::login($user,true);

            $this->setSessionData('sso_user', $user->email);
            $this->userInfo();
        }else{
        	header('Content-type: application/json; charset=UTF-8');
        	echo json_encode($user);
        }
    }

    public function sociallogin()
    {
        $this->startBrokerSession();

        if (empty($_POST['provider'])) $this->fail("No provider specified", 400);
        if (empty($_POST['returnUrl'])) $this->fail("No returnUrl specified", 400);
        
        header('Content-type: application/json; charset=UTF-8');
        echo json_encode(Socialite::driver($_POST['provider'])->redirectUrl($_POST['returnUrl'])->redirect()->getTargetUrl());
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function social()
    {
        $this->startBrokerSession();

        $user = Socialite::driver($_POST['provider'])->redirectUrl($_POST['returnUrl'])->stateless()->user();

        $authUser = $this->findOrCreateUser($user, $_POST['provider']);

        if(!$authUser->photo){
            // $f = explode('/',$authUser->photo);
            // unlink(public_path('profile_images/'.end($f)));

            $path = $user->avatar_original;
            $fileName = time().rand(111,999).'.'.'jpg';
            Image::make($path)->save(public_path('profile_images/' . $fileName));             
            $authUser->photo= url('/').'/profile_images/'.$fileName;
            $authUser->save();
        }
        
        if($authUser->verified){

            Auth::login($authUser,true);

            $this->setSessionData('sso_user', $authUser->email);
            $this->userInfo();
        }else{
            header('Content-type: application/json; charset=UTF-8');
            echo json_encode($authUser);
        }
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        if(User::where('email', $user->email)->exists()){
            $u = User::where('email', $user->email)->where('provider', null)->first();
            if($u){
                $u->provider_id = $user->id;
                $u->provider = $provider;
                $u->verified = 1;
                $u->save();

                return $u;
            }
            $u = User::where('email', $user->email)->where('provider','!=', null)->first();
            return $u;
        }
        else{
            $u = new User;
            $u->provider = $provider;
            $u->provider_id = $user->getId();
            $u->name = $user->getName();
            $u->email = $user->getEmail();
            $u->designation = null;
            if($user->getEmail())
                $u->verified = 1;
            else{
                $u->confirmation_code = Str::random(60);
            }
            $u->save();
            return $u;
        }
    }

    /**
     * Get the user information
     *
     * @return array
     */
    protected function getUserInfo($username)
    {
        $user = User::where('email',$username)->first();

        return $user ? $user : null;
    }

    public function getUserById($id){
        return User::findOrFail($id);
    }

    public function inneruserInfo()
    {
        $username = $this->getSessionData('sso_user');

        if($username){
            $user = User::where('email',$username)->first();
            Auth::login($user,true);
        }
        else{
            $token = '';
            $broker = null;

            foreach (MySSOServer::$brokers as $id => $secret) {
                $cookie = 'sso_token_' . preg_replace('/[_\W]+/', '_', strtolower($id));
                if(array_key_exists($cookie,$_COOKIE)){
                    $token = $_COOKIE[$cookie];
                    $broker = $id;
                    $secret = $secret['secret'];
                    break;
                }
            }

            if($broker){
                $sid = $this->generateSessionId($broker, $token);

                $linkedId = $this->cache->get($sid);
                
                session_id($linkedId);
                session_start();
                
                // if(Auth::check()){
                //     $this->setSessionData('sso_user', Auth::user()->email);
                // }
                // else{
                    $user = null;

                    $username = $this->getSessionData('sso_user');
                    
                    if ($username) {
                        $user = User::where('email',$username)->first();
                        Auth::login($user,true);
                    }
                // }
            }
        }
    }

    public function checklogin(){

        $this->startBrokerSession();

        $username = $this->getSessionData('sso_user');
        $user = User::where('email',$username)->first();

        if (!$username) {
            if($_POST['token'] == 0)
                $user = User::where('id',$_POST['id'])->where('verified',1)->first();
            else
                $user = User::where('id',$_POST['id'])->where('remember_token',$_POST['token'])->where('verified',1)->first();
            if($user){
                $this->setSessionData('sso_user', $user->email);
            } 
        }

        header('Content-type: application/json; charset=UTF-8');
        echo json_encode($user);
    }
}