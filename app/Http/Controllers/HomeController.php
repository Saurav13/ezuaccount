<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Image;
use Session;

class HomeController extends Controller
{
    public function index()
    {
        
        return view('home');
    }
    public function editProfile()
    {
        return view('manageprofile');
    }
    public function getInfo(Request $request)
    {
        return User::findOrFail(Auth::user()->id);
    }
    public function saveInfo(Request $request)
    {
      $request->validate([
       'value' => 'required' ,
        
    ]);
        User::where('id',Auth::user()->id)->update([$request->key=>$request->value]);

        return User::findOrFail(Auth::user()->id);
    }
    public function savePastWork(Request $request)
    {
    //    dd($request->key,$request->value,$request->user_id);
    // dd(json_decode($request->value));
        $request->validate([
            'value' => 'required' ,
            
        ]);
        $vals=[];
        foreach(json_decode($request->value) as $v)
        {
            if($v=="")
                continue;
            $vals[]=$v;
        }
        User::where('id',Auth::user()->id)->update([$request->key=>json_encode($vals)]);
        return User::findOrFail(Auth::user()->id);
    }
    public function savePastEducation(Request $request)
    {
        // dd("asdasd");
        //    dd($request->key,$request->value,$request->user_id);
        $request->validate([
            'value' => 'required' ,
            
        ]);
        $vals=[];
        foreach(json_decode($request->value) as $v)
        {
            if($v=="")
                continue;
            $vals[]=$v;
        }
        User::where('id',Auth::user()->id)->update([$request->key=>json_encode($vals)]);

        return User::findOrFail(Auth::user()->id);
    }
    public function saveImage(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:jpeg,jpg,png,gif,svg|max:10000' ,       
        ]);
        if($request->file('file'))
        {
            if(User::findOrFail(Auth::user()->id)->photo)
            {
                try{
                    $f = explode('/',User::findOrFail(Auth::user()->id)->photo);
                    unlink(public_path('profile_images/'.end($f)));
                }catch(\Exception $e)
                {

                }
            }
            $file=$request->file('file');
            $destinationPath = public_path().'/'.'profile_images';
            
            
            $filename=time().rand(111,000).'_'.$file->getClientOriginalName();
            Image::make($file)->resize(400,400)->save($destinationPath.'/'.$filename);
            User::where('id',Auth::user()->id)->update(['photo'=>url('/').'/profile_images'.'/'.$filename]);
        }           
        return User::findOrFail(Auth::user()->id);
    }
    public function saveGeneralInfo(Request $request)
    {
        // dd($request->all());
        $user=User::findOrFail(Auth::user()->id);
        if($request->designation)
            $user->designation=$request->designation;
        if($request->organization)
            $user->organization=$request->organization;
        if($request->dob)
            $user->dob=$request->dob;
        if($request->gender)
            $user->gender=$request->gender;
        $user->save();

        return $user;
    }
    public function saveExtraInfo(Request $request)
    {
        // dd($request->all());
        $user=User::findOrFail(Auth::user()->id);
        $user->about=$request->about;
        $user->phone=$request->phone;
        $user->skill=$request->skill;
        $user->save();

        return $user;
    }

    public function changePassword(Request $request)
    {
        $user = User::find(Auth::user()->id);
        
        if(!$user->password){
            $request->validate([
                'password' => 'required|min:6|confirmed',
            ]);
        }
        else{
            $request->validate([
                'oldPassword' => 'required',
                'password' => 'required|min:6|confirmed',
            ]);
        }

        if(!$user->password){
            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['success'=>'Password Changed'], 200);
        }
        else{
            if (Auth::attempt(['email' => Auth::user()->email, 'password' => $request->oldPassword]))
            {
                $user->password = bcrypt($request->password);
                $user->save();

                return response()->json(['success'=>'Password Changed'], 200);
            }
            else{
                return response()->json(['errors'=>['oldPassword'=>[0 =>'Incorrect password']]], 422);
            }
        }
    }
    public function logout($request)
    {
        $broker  =new MyBroker();
        $broker->logout();
        $this->guard()->logout();
    
        $request->session()->flush();
    
        $request->session()->regenerate();
    
        return redirect('/');
    }
}
