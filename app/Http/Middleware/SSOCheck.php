<?php

namespace App\Http\Middleware;

use Closure;
use App\MySSOServer;
use Auth;

class SSOCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $request->query('redirectUrl') ? $request->query('redirectUrl') : env('HUB_URL');
        
        if (Auth::guest()){
            $myssoserver = new MySSOServer;
            $myssoserver->inneruserInfo();
            
            if(!Auth::check())
                return redirect($url);
        }
        // dd(Auth::user());
        return $next($request);
    }
}
